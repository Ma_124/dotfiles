# shellcheck shell=sh

# Set the XDG base directories to their defaults (only for convenience)
if [ -n "$HOME" ]; then
    export XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
    export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
    export XDG_STATE_HOME="${XDG_STATE_HOME:-$HOME/.local/state}"
    export XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"
fi


# Setup some general env variables
export GOPATH="${GOPATH:-"$HOME/.go"}"

[ -n "$(command -v dotnet)" ] && export DOTNET_CLI_TELEMETRY_OPTOUT=1

# Setup Path
# add dir to path if it exists and is not already contained
path_add() {
    if [ -e "$1" ]; then
        case ":$PATH:" in
            *":$1:"*) : ;;
            *) PATH="$1:$PATH"
        esac
    fi
}

# PATH members in reversed order
path_add "$HOME/.local/share/JetBrains/Toolbox/scripts"
path_add "$HOME/.emacs.d/bin"
path_add "$HOME/.config/emaacs/bin"
path_add "$HOME/.yarn/bin"
path_add "/usr/local/go/bin"
[ -n "$GOPATH" ] && path_add "$GOPATH/bin"
path_add "$HOME/.cargo/bin"
path_add "$HOME/.opt/bin"
path_add "$HOME/.local/bin"
export PATH

if [ -e /home/ma/.nix-profile/etc/profile.d/nix.sh ];
    then . /home/ma/.nix-profile/etc/profile.d/nix.sh;
fi

# Allow for overrides of later variables
# ignore distro's defaults
unset TERMINAL EDITOR VISUAL
# shellcheck source=/dev/null
[ -e "$HOME/.custom_profile" ] && . "$HOME/.custom_profile"


# Choose a terminal emulator
if [ -z "$TERMINAL" ]; then
    if [ -x /usr/bin/kitty ]; then
        export TERMINAL_NW="/usr/bin/kitty --single-instance"
        export    TERMINAL="/usr/bin/kitty --single-instance --detach --wait-for-single-instance-window-close"
    elif [ -x /usr/bin/gnome-terminal ]; then
        export TERMINAL_NW="/usr/bin/gnome-terminal --"
        export    TERMINAL="/usr/bin/gnome-terminal --wait --"
    elif [ -x /usr/bin/konsole ]; then
        export TERMINAL="/usr/bin/konsole -e"
    elif [ -x /usr/bin/xfce4-terminal ]; then
        export TERMINAL="/usr/bin/xfce4-terminal -x"
    elif [ -x /usr/bin/xterm ]; then
        export TERMINAL="/usr/bin/xterm -e"
    fi
fi


# Choose an editor
if [ -z "$EDITOR" ] || [ -z "$VISUAL" ]; then
    if [ -x /usr/bin/emacsclient ]; then
        : "${EDITOR:=emacsclient -t}"
        if [ -z "$VISUAL" ]; then
            VISUAL="emacsclient -c"
            VISUAL_NW="$VISUAL -n"
        fi
    elif [ -x /usr/bin/helix ]; then
        : "${EDITOR:=helix}"
    elif [ -x /usr/bin/nvim ]; then
        : "${EDITOR:=nvim}"
    elif [ -x /usr/bin/vim ]; then
        : "${EDITOR:=vim}"
    elif [ -x /usr/bin/vi ]; then
        : "${EDITOR:=vi}"
    elif [ -x /usr/bin/nano ]; then
        : "${EDITOR:=nano}"
    fi

    if [ -z "$VISUAL" ] && [ -n "$EDITOR" ] && [ -n "$TERMINAL" ]; then
        export VISUAL="$TERMINAL $EDITOR"
        [ -n "$TERMINAL_NW" ] && export VISUAL_NW="$TERMINAL_NW $EDITOR"
    fi
fi
# only testing for the DISPLAY family causes issues with wayland on fedora, so check for SSH as well
[ -z "$DISPLAY$WAYLAND_DISPLAY" ] && [ -n "$SSH_CLIENT$SSH_TTY" ] && unset VISUAL VISUAL_NW
export EDITOR VISUAL VISUAL_NW

# Special case GIT_EDITOR for now, until emacs works again
export GIT_EDITOR=nvim

# Uncomment, if there's no internet connection
#export CARGO_NET_OFFLINE=true

# Choose a browser
if [ -z "$BROWSER" ]; then
    [ -x /usr/bin/firefox ] && export BROWSER=/usr/bin/firefox
    [ -x /usr/bin/firefox-developer-edition ] && export BROWSER=/usr/bin/firefox-developer-edition
fi

# Attach to an ssh-agent
[ -z "$SSH_AUTH_SOCK" ] && export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"


# Choose a container engine
[ -z "$CONTAINER_ENGINE" ] && \
    if [ -x /usr/bin/podman ]; then
        export CONTAINER_ENGINE="podman"
    elif [ -x /usr/bin/docker ]; then
        export CONTAINER_ENGINE="sudo docker"
    else
        export CONTAINER_ENGINE="false"
    fi

if [ -e ~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu ]; then
    export MANPATH="$HOME/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/share/man:$MANPATH"
elif [ -e ~/.rustup/toolchains/stable-x86_64-unknown-linux-gnu ]; then
    export MANPATH="$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/man:$MANPATH"
fi

# Use kitty for diffs (but not over serial)
[ "$TERM" = "xterm-kitty" ] && export DIFFPROG="kitty kitten diff"

# Assume any incoming serial connection is connected to a kitty instance
case "$(tty)" in
    /dev/ttyS*)
        export TERM=xterm-kitty
        stty rows 50 cols 93
        ;;
esac

