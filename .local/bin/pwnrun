#!/usr/bin/env shebang-c
#include <stdbool.h>
#include <stdio.h>
#include <sys/personality.h>
#include <sys/prctl.h>
#include <unistd.h>

void usage(char *cmd) {
  printf("usage: %s [flags] <exe>\n", cmd);
  puts("");
  puts("  -h      Print this help.");
  puts("  -R      Disable ASLR.");
  puts("  -t      Allow this process to be traced after launch.");
  puts("  -c=CMD  argv[0] to pass to the process.");
  puts("  -C      pass an empty argv to the process.");
}

int main(int argc, char *argv[]) {
  char opt;

  bool no_aslr = false;
  bool trace = false;
  char *fake_cmd = NULL;
  bool no_cmd = false;

  while ((opt = getopt(argc, argv, "hRtc:C")) != -1) {
    switch (opt) {
    case 'h':
      usage(argv[0]);
      return 0;
    case 'R':
      no_aslr = true;
      break;
    case 't':
      trace = true;
      break;
    case 'c':
      fake_cmd = optarg;
      break;
    case 'C':
      no_cmd = true;
      break;
    default:
      usage(argv[0]);
      return -1;
    }
  }

  if (optind == argc) {
    usage(argv[0]);
    return -1;
  }
  char *real_cmd = argv[optind];

  if (fake_cmd) {
    argv[optind] = fake_cmd;
  }

  if (no_cmd) {
    if (optind + 1 == argc) {
      argv[optind] = NULL;
    } else {
      puts("-C conflicts with arguments");
      return -1;
    }
  }

  if (no_aslr) {
    int persona = personality(0xffffffff);
    if (persona < 0) {
      perror("query personality");
      return -1;
    }
    persona |= ADDR_NO_RANDOMIZE;
    persona = personality(persona);
    if (persona < 0) {
      perror("set personality");
      return -1;
    }
  }

  if (trace) {
    prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY, 0, 0, 0);
  }

  if (execvp(real_cmd, &argv[optind]) < 0) {
    perror("execvp");
  }
}
