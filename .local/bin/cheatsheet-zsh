#!/bin/zsh

if bat -L | grep -Fq zsh; then
    alias cat="bat -pl zsh"
fi

cat << 'EOF'
# === PARAMETER EXPANSION zshexpn ===
# Use default if unset
${var:-default}

# Assign default if unset
${var:=default}

# Error if unset
${var:?error message}

# Strip pre-/suffix
${var#prefix regex}
${var%suffix regex}

# Substitute
 ${var/regex/repl} # (first)
${var//regex/repl} # (global)
${var:/regex/repl} # (whole string)

# Capitalize / Lowercase / Uppercase
${(C)var}
${(L)var}
${(U)var}

# Head / Dirname
${var:h}
# Tail / Basename
${var:t}

# === FILENAME GENERATION zshexpn ===
# = with EXTENDED_GLOB
# Case insensitive
(#i)**/*

# Allow zero matches
**/*(N)

# Match dotfiles
**/*(D)

# Sort numerically
**/*(n)


# === CONDITIONAL EXPRESSIONS zshmisc ===
# String is empty (and inverse)
[[ -z "$var" ]]
[[ -n "$var" ]]

# Strings match (and inverse)
[[ "$var" == "string" ]]
[[ "$var" != "string" ]]

# Regex matches
[[ "$var" =~ regex ]]

# File exists
[[ -e file ]]

# Is regular file / directory
[[ -f file ]]
[[ -d file ]]
EOF
