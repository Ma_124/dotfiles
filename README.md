# Ma_124's Dotfiles

## Installation
```zsh
yadm clone https://gitlab.com/Ma_124/dotfiles.git
```

Allow execution of bootstrap.

## Quick Links
- [Doom Emacs](.config/doom/config.org)
- [Scripts](.local/bin/)
- [`.zshrc`](.config/zsh/)
