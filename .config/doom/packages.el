;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)

;(package! tree-sitter
;  :ignore (null (bound-and-true-p module-file-suffix))
;  :pin "5e1091658d625984c6c5756e3550c4d2eebd73a1")
;(package! tree-sitter-langs
;  :ignore (null (bound-and-true-p module-file-suffix))
;  :pin "4b9a0cc616bd849bb97b21a30745b3da654e13d6")
;
;(package! transient
;  :pin "c2bdf7e12c530eb85476d3aef317eb2941ab9440"
;  :recipe (:host github :repo "magit/transient"))
;
;(package! with-editor
;  :pin "bbc60f68ac190f02da8a100b6fb67cf1c27c53ab"
;  :recipe (:host github :repo "magit/with-editor"))

(package! nasm-mode :pin "65ca6546fc395711fac5b3b4299e76c2303d43a8")
(package! x86-lookup :pin "1573d61cc4457737b94624598a891c837fb52c16")
(package! caddyfile-mode :pin "fc41148f5a7eb320f070666f046fb9d88cf17680")
(package! wgsl-mode :pin "e7856d6755d93e40ed74598a68ef5f607322618b")
(package! capnp-mode :pin "f7fccad7d737f77896211bec1173117497634143")
;(package! j-mode :pin "e8725ac8af95498faabb2ca3ab3bd809a8f148e6")

(package! ox-mahtml
  :recipe (:host gitlab :repo "Ma_124/ox-mahtml")
  :pin "8b9d869b54809d6c99e201f99af1b59d3557a1e6")

(package! promela-mode
  :recipe (:host github :repo "rudi/promela-mode")
  :pin "905559f430f16e16cd12dac40b59f5b613c026fc")

;(package! ox-mahtml
;  :recipe (:local-repo "~/src/ma124/ox-mahtml"
;           :files ("*.el" "dist.css")))
;(package! ox-bash-interactive
;  :recipe (:local-repo "~/src/ma124/ox-bash-interactive"
;           :files ("*.el")))
