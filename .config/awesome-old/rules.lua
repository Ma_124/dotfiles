screen_secondary = awful.screen.getbycoord(2000, 1080)

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            -- screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "copyq",  -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin",  -- kalarm.
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer",
                "spectacle",
            },

            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester",  -- xev.
            },
            role = {
                "AlarmWindow",   -- Thunderbird's calendar.
                "ConfigManager", -- Thunderbird's about:config.
                "pop-up",        -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = {type = { "normal", "dialog" }},
        properties = { titlebars_enabled = true }},
    {
        rule = { maximized = true },
        properties = { maximized = false },
    },

    -- Others
    -- Lua pattern syntax: http://www.lua.org/manual/5.3/manual.html#6.4.1
    {
        rule_any = { class = { "discord", "Element" } },
        properties = { screen = screen_secondary, tag = "9", maximized=false },
    },
    {
        rule_any = { name = { "Picture-in-Picture", "Picture in picture" } },
        properties = { floating = true, ontop = true, sticky = true },
    },
    {
        rule = { class = "Qalculate-gtk" },
        properties = { floating = true, ontop = true, sticky = true },
    },
    {
        rule_any = { class = { "Xephyr", "pavucontrol-qt" } },
        properties = { floating = true, ontop = true },
    },
    {
        rule_any = { class = { "dolphin", "Thunar" } },
        properties = { screen = screen_secondary, tag = "6" },
    },
    {
        rule = { class = "Steam" },
        properties = { screen = screen_secondary, tag = "8" },
    },
    {
        rule = { class = "permanent-kitty" },
        properties = { screen = screen_secondary, tag = "2" },
    },
}

-- Move to rules for v4.4
for _, preset in pairs(naughty.config.presets) do
    preset.screen = screen_secondary
    preset.position = "top_left"
end

-- Launch Once
if not gears.filesystem.file_readable("/tmp/awesome-started") then
    awful.spawn("touch /tmp/awesome-started")
    awful.spawn("conky")

    -- awful.spawn(terminal)
    awful.spawn("kitty --single-instance --detach --class permanent-kitty")
    awful.spawn("keepassxc")

    awful.spawn("discord")
    awful.spawn("element-desktop")
    -- awful.spawn("cantata")

    awful.spawn("systemctl --user start ssh-agent")
    awful.spawn("systemctl --user start emacs")

    -- awful.spawn(browser)
end
