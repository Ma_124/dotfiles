pcall(require, "luarocks.loader")

gears = require("gears")
awful = require("awful")
require("awful.autofocus")
wibox = require("wibox")
beautiful = require("beautiful")
naughty = require("naughty")
menubar = require("menubar")
hotkeys_popup = require("awful.hotkeys_popup")
-- require("awful.hotkeys_popup.keys") -- Show keys for TMUX, VIM etc

function source(path)
    dofile(gears.filesystem.get_dir("config") .. "/" .. path)
end

-- Check for startup errors
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title  = "Startup Errors",
        text   = awesome.startup_errors
    })
end

-- Handle errors during execution
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Error",
            text = tostring(err)
        })
        in_error = false
    end)
end

-- Themes define colours, icons, font and wallpapers.
if not beautiful.init(gears.filesystem.get_dir("config") .. "/themes/" .. "default/theme.lua") then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title  = "Error",
        text   = "cannot read theme, falling back to default"
    })
    -- Safe Fallback
    beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
end

-- Variables
terminal = os.getenv("TERMINAL_NW") or os.getenv("TERMINAL") or "/usr/bin/xterm"
visual = os.getenv("VISUAL_NW") or os.getenv("VISUAL") or terminal .. (os.getenv("EDITOR") or "vim")
browser = os.getenv("BROWSER")
modkey = "Mod4"

-- Menu
source("menu.lua")

-- Keybinds
source("keys.lua")

-- Rules
source("rules.lua")

-- Manage (e.g. Titlebars)
source("manage.lua")

-- Focus follows Mouse TODO
client.connect_signal("mouse::enter", function(c)
    if c.class == "Peek" then
        return
    end
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

awful.spawn(".config/picom/launch.sh")
awful.spawn("launch-redshift")

math.randomseed(os.time())
if math.random(1, 100) == 1 then
    awful.spawn("feh --bg-scale --no-xinerama Pictures/alt-wallpaper.jpg")
end
