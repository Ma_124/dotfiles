-- Map mouse button names to numbers
-- Main Mouse Button (Left)
local mouse_main   = 1
-- Secondary Mouse Button (Right)
local mouse_sec    = 3
-- Scroll Wheel
local mouse_middle = 2
local mouse_up     = 4
local mouse_down   = 5

local rofi_launcher = "/home/ma/.config/rofi/launchers/type-3/launcher.sh"

globalkeys = gears.table.join(
    awful.key({ modkey, },           "s",      hotkeys_popup.show_help,   { description = "show help",     group = "awesome" }),
    awful.key({ modkey, "Control" }, "Left",   awful.tag.viewprev,        { description = "view previous", group = "tag" }),
    awful.key({ modkey, "Control" }, "Right",  awful.tag.viewnext,        { description = "view next",     group = "tag" }),
    awful.key({ modkey, },           "Escape", awful.tag.history.restore, { description = "go back",       group = "tag" }),

    awful.key({ modkey, }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        { description = "focus next by index", group = "client" }
    ),
    awful.key({ modkey, }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        { description = "focus previous by index", group = "client" }
    ),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end, { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end, { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end, { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end, { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,                      { description = "jump to urgent client", group = "client" }),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        { description = "go back", group = "client" }),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,               { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey, "Shift"   }, "Return", function () awful.spawn(visual) end,                 { description = "open a visual editor", group = "launcher" }),
    awful.key({ modkey, "Control" }, "r",      awesome.restart,                                     { description = "reload awesome",  group = "awesome" }),
    awful.key({ modkey, "Shift"   }, "e",      function ()
            awful.spawn("quit-awesome");
        end, { description = "quit awesome", group = "awesome" }),

    awful.key({ modkey,           }, "z",      function () awful.tag.incmwfact( 0.05)          end, { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey,           }, "h",      function () awful.tag.incmwfact(-0.05)          end, { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift"   }, "h",      function () awful.tag.incnmaster( 1, nil, true) end, { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift"   }, "z",      function () awful.tag.incnmaster(-1, nil, true) end, { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Control" }, "h",      function () awful.tag.incncol( 1, nil, true)    end, { description = "increase the number of columns", group = "layout" }),
    awful.key({ modkey, "Control" }, "z",      function () awful.tag.incncol(-1, nil, true)    end, { description = "decrease the number of columns", group = "layout" }),
    awful.key({ modkey,           }, "space",  function () awful.layout.inc( 1, awful.screen.focused(), layouts)                end, {description = "select next", group = "layout" }),
    awful.key({ modkey, "Shift"   }, "space",  function () awful.layout.inc(-1, awful.screen.focused(), layouts)                end, {description = "select previous", group = "layout" }),

    awful.key({ modkey, "Control" }, "n",
        function ()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", { raise = true }
                )
            end
        end,
        { description = "restore minimized", group = "client" }),

    -- Prompt
    awful.key({ modkey }, "d",
        function ()
            awful.spawn(rofi_launcher)
        end,
        { description = "run prompt", group = "launcher" }),
    awful.key({ modkey }, "r",
        function ()
            awful.spawn(rofi_launcher)
        end,
        { description = "run prompt", group = "launcher" }),
    awful.key({ modkey }, "l",
        function ()
            awful.spawn("lock")
        end,
        { description = "lock screen", group = "awesome" }),
    awful.key({ modkey, "Shift" }, "l",
        function ()
            awful.spawn("lock --nopause")
        end,
        { description = "lock screen with audio playback", group = "awesome" }),

    awful.key({}, "Print",
        function ()
            awful.spawn("spectacle -i -m")
        end,
        { description = "Screenshot current monitor", group = "launcher" }),
    awful.key({ modkey }, "Print",
        function ()
            awful.spawn("spectacle -i --region")
        end,
        { description = "Screenshot region", group = "launcher" }),
    awful.key({ modkey }, "c",
        function ()
            awful.spawn("qalculate-gtk")
        end,
        { description = "Launch Qalculate!", group = "launcher" }),

    -- Media Keys
    awful.key({},          "XF86AudioRaiseVolume", function() os.execute("playerctl-helper raise")         end, { description = "raise volume", group = "media" }),
    awful.key({},          "XF86AudioLowerVolume", function() os.execute("playerctl-helper lower")         end, { description = "lower volume", group = "media" }),
    awful.key({},          "XF86AudioMute",        function() os.execute("playerctl-helper toggle-mute")   end, { description = "mute audio", group = "media" }),
    awful.key({},          "XF86AudioPlay",        function() awful.spawn("playerctl-helper toggle-pause") end, { description = "play/pause media", group = "media" }),
    awful.key({ modkey },  "p",                    function() awful.spawn("playerctl-helper toggle-pause") end, { description = "play/pause media", group = "media" }),
    awful.key({ modkey, "Shift" }, "p",            function() awful.spawn("playerctl-helper pause-all")    end, { description = "pause all media playback", group = "media" }),
    awful.key({ "Shift" }, "XF86AudioPlay",        function() awful.spawn("playerctl-helper pause-all")    end, { description = "pause all media playback", group = "media" }),
    awful.key({},          "XF86AudioNext",        function() awful.spawn("playerctl-helper next")         end, { description = "play next media", group = "media" }),
    awful.key({},          "XF86AudioPrev",        function() awful.spawn("playerctl-helper prev")         end, { description = "play previous media", group = "media" })
)

-- Used in rules.lua
clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "Toggle fullscreen", group = "client" }),
    awful.key({ modkey, "Shift"   }, "q",      function (c) c:kill() end,                         { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle,                      { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end, { description = "move to master", group = "client" }),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end, { description = "move to screen", group = "client" }),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end, { description = "toggle keep on top", group = "client" }),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end,
        { description = "minimize", group = "client" }),
    awful.key({ modkey, }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "(un)maximize", group = "client" }),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        { description = "(un)maximize vertically", group = "client" }),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        { description = "(un)maximize horizontally", group = "client" }),
    awful.key({ modkey, "Shift"   }, "s",
        function (c)
            c.sticky = not c.sticky
            c:raise()
        end,
        { description = "toggle sticky", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key(
            { modkey },
            "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            i == 1 and { description = "view tag", group = "tag" } or {}),
        -- Toggle tag display.
        awful.key(
            { modkey, "Control" }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            i == 1 and { description = "toggle tag", group = "tag" } or {}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
               end
            end,
            i == 1 and { description = "move focused client to tag", group = "tag" } or {}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            i == 1 and { description = "toggle focused client on tag", group = "tag" } or {})
    )
end


for i, dir in ipairs({ "Up", "Down", "Left", "Right" }) do
    clientkeys = gears.table.join(clientkeys,
        awful.key({ modkey }, dir,
            function (c)
                awful.client.focus.bydirection(string.lower(dir), c)
            end,
            { description = "Focus by direction", group = "client" }),
        awful.key({ modkey, "Shift" }, dir,
            function (c)
                awful.client.swap.bydirection(string.lower(dir), c)
            end,
            { description = "Swap by direction", group = "client" })
    )
end

-- Used in rules.lua
clientbuttons = gears.table.join(
    awful.button({}, mouse_main, function (c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, mouse_main, function (c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, mouse_sec, function (c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
