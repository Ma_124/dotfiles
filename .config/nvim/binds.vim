" Exit Insert Mode in Terminal with ESC
tnoremap <Esc> <C-\><C-n>

" Move up and down through wrapped lines
" without jumping
map <silent> <Up> gk
imap <silent> <Up> <C-o>gk
map <silent> <Down> gj
imap <silent> <Down> <C-o>gj

