" Quality of Life Plugins
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
Plug 'godlygeek/tabular'
Plug 'ctrlpvim/ctrlp.vim'
let g:ctrlp_cmd = 'CtrlPBuffer'
Plug 'vim-scripts/VisIncr'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-gitgutter'
Plug 'justinmk/vim-sneak'

" Syntastic
Plug 'vim-syntastic/syntastic'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_ignore_files = ['.asm$', '.s$', '.S$']

" Nerdtree
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'ryanoasis/vim-devicons', { 'on': 'NERDTreeToggle' }
Plug 'tiagofumo/vim-nerdtree-syntax-highlight', { 'on': 'NERDTreeToggle' }
Plug 'scrooloose/nerdtree-git-plugin', { 'on': 'NERDTreeToggle' }

let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
map <C-n> :NERDTreeToggle<CR>

" Language Support
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
let g:vim_markdown_folding_disabled = 1

Plug 'lervag/vimtex', { 'for': 'tex' }
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
autocmd Filetype tex setl updatetime=1

Plug 'chr4/nginx.vim', { 'for': 'nginx' }

Plug 'cespare/vim-toml', { 'for': 'toml' }
Plug 'maralla/vim-toml-enhance', { 'for': 'toml' }

Plug 'mattn/emmet-vim', { 'for': 'html' }
"Plug 'kylelaker/riscv.vim'
Plug '/home/ma/src/ma124/riscv.vim'

Plug 'dmix/elvish.vim', { 'on_ft': ['elvish']}

Plug 'sile-typesetter/vim-sile'

" Theming
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_powerline_fonts = 1

" Color Codes (Hex, W3C, X)
Plug 'chrisbra/Colorizer'

" TODO LSPs
