autocmd BufRead,BufNewFile *.colorscheme set filetype=dosini
autocmd BufRead,BufNewFile *.unity       set filetype=yaml
autocmd BufRead,BufNewFile *.meta        set filetype=yaml
autocmd BufRead,BufNewFile *.mat         set filetype=yaml
autocmd BufRead,BufNewFile *.har         set filetype=json
autocmd BufRead,BufNewFile *PKGBUILD*    set filetype=PKGBUILD

autocmd BufNewFile *.h              call append(0, '#pragma once')
autocmd BufNewFile *.py             call append(0, '#!/usr/bin/env python3')
autocmd BufNewFile *.zsh            call append(0, '#!/bin/zsh')
autocmd BufNewFile *.sh             call append(0, '#!/bin/zsh')
autocmd BufNewFile **/.local/bin/*  call append(0, '#!/bin/zsh')
autocmd BufNewFile **/.local/bin/*  set filetype=zsh

autocmd BufNewFile *.tex      0read `=expand('<sfile>:p:h') .. '/templates/main.tex'`
autocmd BufNewFile *.html     0read `=expand('<sfile>:p:h') .. '/templates/index.html'`
autocmd BufNewFile *PKGBUILD* 0read `=expand('<sfile>:p:h') .. '/templates/PKGBUILD'`
autocmd BufNewFile deny.toml  0read `=expand('<sfile>:p:h') .. '/templates/deny.toml'`

