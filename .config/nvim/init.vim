" START of modified sensible.vim
" Copyright © Tim Pope. Distributed under the same terms as Vim itself.
" See :help license.
filetype plugin indent on
syntax enable

set autoindent
set backspace=indent,eol,start
set complete-=i
set smarttab

set nrformats-=octal

if !has('nvim') && &ttimeoutlen == -1
    set ttimeout
    set ttimeoutlen=100
endif

set incsearch
" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
    nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

set laststatus=2
set ruler
set wildmenu

if !&scrolloff
    set scrolloff=1
endif
if !&sidescrolloff
    set sidescrolloff=5
endif
set display+=lastline

if &encoding ==# 'latin1' && has('gui_running')
    set encoding=utf-8
endif

if &listchars ==# 'eol:$'
    set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

if v:version > 703 || v:version == 703 && has("patch541")
    set formatoptions+=j " Delete comment character when joining commented lines
endif

if has('path_extra')
    setglobal tags-=./tags tags-=./tags; tags^=./tags;
endif

set autoread

if &history < 1000
    set history=1000
endif
if &tabpagemax < 50
    set tabpagemax=50
endif
if !empty(&viminfo)
    set viminfo^=!
endif
set sessionoptions-=options

" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^linux\|^Eterm'
    set t_Co=16
endif

inoremap <C-U> <C-G>u<C-U>
" END OF sensible.vim

set hidden
set et
set ts=4
set sw=4
set expandtab
set ignorecase
set nu
set scrolloff=10
set sidescrolloff=999

" Change LineNr color and highlight trailing spaces
function! s:ExtraHighlights() abort
    "highlight LineNr ctermfg=grey
    "highlight TrailingWhitespace ctermbg=red guibg=red
endfunction

augroup ExtraHighlights
    "autocmd ColorScheme * call ExtraHighlights()
    "autocmd BufRead,BufNewFile * match TrailingWhitespace /\s\+$/
    "autocmd InsertEnter * match TrailingWhitespace /\s\+\%#\@<!$/
    "autocmd InsertLeave * match TrailingWhitespace /\s\+$/
augroup END

" Install Plug
" XXX if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
" XXX     silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" XXX     autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
" XXX endif

" XXX call plug#begin('~/.local/share/nvim/plugged')
" XXX   source ~/.config/nvim/plugins.vim
" XXX call plug#end()

if !exists('g:vscode')
    source ~/.config/nvim/util.vim
    source ~/.config/nvim/filetypes.vim
    source ~/.config/nvim/binds.vim
endif
