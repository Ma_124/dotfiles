if iszsh; then
    zmodload zsh/terminfo

    # Emcas mode
    bindkey -e

    # Home and End key
    bindkey '^[[7~' beginning-of-line
    bindkey '^[[H' beginning-of-line
    if [[ "${terminfo[khome]}" != "" ]]; then
      bindkey "${terminfo[khome]}" beginning-of-line
    fi

    bindkey '^[[8~' end-of-line
    bindkey '^[[F' end-of-line
    if [[ "${terminfo[kend]}" != "" ]]; then
      bindkey "${terminfo[kend]}" end-of-line
    fi

    bindkey '^[[3~' delete-char
    bindkey '^[[C'  forward-char
    bindkey '^[[D'  backward-char

    # Navigate words with ctrl+arrow keys
    bindkey '^[Oc' forward-word
    bindkey '^[Od' backward-word
    bindkey '^[[1;5D' backward-word
    bindkey '^[[1;5C' forward-word
    bindkey '^H' backward-kill-word

    # Shift+Tab Undo
    bindkey '^[[Z' undo

    function __clear_and_cd() {
        cd
        clear
        zle redisplay
        zle reset-prompt
    }
    zle -N clear-and-cd __clear_and_cd

    bindkey '^[OP' clear-and-cd
fi
