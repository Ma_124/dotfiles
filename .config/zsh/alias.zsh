# Default Options
alias l="ls -B --group-directories-first --color=always"
alias ll="ls -alhBG --group-directories-first --color=always"
alias la=ll
alias ö=l
alias öö=ll
alias dc="cd"
[ -x /usr/bin/sl ] && alias sl="sl -ea" || alias sl=ls

alias sysctl="sudo SYSTEMD_LESS=FRSMK systemctl"
alias usrctl="SYSTEMD_LESS=FRSMK systemctl --user"
alias journalctl="SYSTEMD_LESS=FRSMK journalctl"

alias mount="sudo mount"
alias umount="sudo umount"
alias reboot="sudo reboot"

alias cpr="cp -r"
alias hd="hexdump -C"
alias ip="ip -c=auto"
alias downl="curl --fail --location --remote-header-name --remote-name --proto-default https --styled-output -w '%{stderr}\nWritten to: %{filename_effective}\nContent Type: %{content_type}\n'"
alias sd="sd -p"
alias fx="$BROWSER"
alias py="python3"
alias fsi="dotnet fsi"
alias flake="nix flake"
alias latex-watch="latexmk -auxdir=/tmp/auxdir -shell-escape -interaction=nonstopmode -pdf -pvc -view=none"
alias mycroc="croc --relay ext-srv-0.lan"

if [[ "$TERM" == "xterm-kitty" && -e "/usr/share/terminfo/x/xterm-kitty-nobg" ]]; then
    alias lynx="TERM=xterm-kitty-nobg lynx"
fi

function x() {
    for f in "$@"; do
        xdg-open "$f" &> /dev/null &!
    done
}

function xopp() {
    for f in "$@"; do
        xournalpp "$f" &> /dev/null &!
    done
}

function cdc() {
    cd "$(clippaste)"
}

function mkcd() {
    if [[ "$@" != "$1" ]]; then
        echo "usage: mkcd <dir>"
        return
    fi
    mkdir "$1"
    cd "$1"
}

function source-env() {
    set -o allexport
    source "${1:-.env}"
    set +o allexport
}

function cmdline() {
    awk \
        'BEGIN {RS="\0"; ORS=""} {if ($0 !~ / /) print $0; else print "\"" $0 "\""; print " "} END {print "\n"}' \
        /proc/"$(pidof $@)"/cmdline
}

function ssh-agent-attach() {
    eval "$(echo /tmp/ssh-XX*/* | awk 'BEGIN {FS="."} {print "export SSH_AUTH_SOCK=" $0 " SSH_AGENT_PID=" $2 }')"
}

function gcr() {
    gcc -o "$1.exe" "$@" && "./$1.exe"
}

function gcc-defs() {
    gcc -E -dM - < "${1:-/dev/null}" | sort -u
}

function disas() {
    objdump -Mintel -d --disassembler-color=on --visualize-jumps=extended-color -C "$@" | less -RF
}

alias readelf="readelf -W"

# Editor
alias v="$EDITOR"
alias snv="sudo -e"
alias hx="helix"
alias nv="nvim"
alias nv-diff="nvim -d"
alias cvi='vim --clean +"set nobackup nowritebackup noundofile noswapfile viminfo="'

# Visual
if [[ -n "$VISUAL" ]]; then
    if [[ -n "$VISUAL_NW" ]]; then
        alias e="$VISUAL_NW"

        # open in existing window
        if [[ "$VISUAL_NW" = emacsclient* ]]; then
            alias em="${VISUAL_NW/ -c/}"
        else
            alias em="$EDITOR"
        fi
    else
        function e() {
            nohup $VISUAL 0<&- &>/dev/null &
        }
        alias em="$EDITOR"
    fi
else
    alias e="$EDITOR"
    alias em="$EDITOR"
fi

function ve() {
    _typ="$(which -w "$1" | awk '{print $2}')"
    # alias, builtin, command, function, hashed, reserved or none
    if [[ "$_typ" == "none" ]]; then
        echo "$1 not found"
    elif [[ "$_typ" == "command" ]]; then
        _path="$(realpath "$(which "$1")")"
        if file -bi "$_path" | grep -Fo 'charset=binary' > /dev/null; then
            echo "$_path:"
            file -b "$_path"
        else
            eval "${VISUAL:-$EDITOR} -n '$_path'"
        fi
    else
        which "$1" | bat -nl zsh
    fi
}

# Pacman
alias pac="paru"
alias paci="paru --repo -S --needed"
alias pacu="paru -Syu --needed"
alias pac-remove="paru -R"
alias pacf="paru -F"
alias pacls="paru -Ql"
alias pac-unused="expac -H M '%-20n\t%10d' \$(paru -Qtdq)"
alias pac-unused-fzf="paru -Qtdq | fzf --preview 'paru -Qi {}' --layout=reverse"
alias pac-browse="paru -Qeq | fzf --preview 'paru -Qi {}' --layout=reverse"
alias pac-foreign="paru -Qetmq | fzf --preview 'paru -Qi {}' --layout=reverse"

# Git
function git-alias() {
    alias "g$1=git $2"
    alias "y$1=yadm $2"
}

git-alias c "commit -v -u"
git-alias cm "commit -v -u -m"
git-alias cf "commit --amend"

git-alias d "diff"
git-alias dw "diff --word-diff=color"
git-alias dt "-c diff.external=difft diff"
git-alias dT "difftool"
git-alias ds "diff --staged"
git-alias dws "diff --staged --word-diff=color"
git-alias dts "-c diff.external=difft diff --staged"
git-alias dTs "difftool"

git-alias dl "diff 'HEAD^'"
git-alias dlt "difftool 'HEAD^'"
[[ -x /usr/bin/bat ]] && git-alias gdh "diff --name-only --diff-filter=d | xargs bat --diff"

git-alias m "merge"
git-alias mt "mergetool"

git-alias a "add"
git-alias an "add -N"
git-alias aa "add --all"
git-alias aan "add --all -N"
git-alias us "reset -N HEAD --"
git-alias us! "restore --staged"

git-alias p "push"
git-alias pu 'push -u origin "$(git branch --show-current)"'
git-alias pup "push upstream"

git-alias pl "pull"
git-alias plup "pull upstream"

git-alias i "init"
git-alias s "status"
git-alias l "log"
git-alias lg "log --all --decorate --oneline --graph"
git-alias sh "show"
git-alias ls "ls-files"

iszsh && unfunction git-alias

function grl() {
    local origins=(${(f)"$(git remote show)"})
    (for remote in $origins; do
        printf "%s" "$remote "
        git remote get-url "$remote"
    done) | column -o ' -> ' -t
}

function grgl() {
    if [[ -z "$1" ]]; then
        echo "usage: grgl <repo name>"
        false
    else
        git remote add origin "git@gitlab.com:Ma_124/$1.git"
    fi
}

function cdr() {
    cd "$(git rev-parse --show-toplevel)"
}


# Yadm
unalias yp
alias yp="yadm push origin HEAD:2020"

function yadm-ignore-changes() {
    echo "Remember to add this command to ~cfg/yadm/bootstrap: "
    echo yadm update-index --skip-worktree "$@"
    echo
    yadm update-index --skip-worktree "$@"
}

# ripgrep
if iszsh; then
    typeset -A rg_suffix rg_expns
    rg_suffix=(
        regex ""
        fixed "f"
        matching ""
        non_matching "v"
        respect_case ""
        ignore_case "i"
    )
    rg_expns=(
        g "rg"
        rg "rg --hidden --ignore-global --glob '!.git'"
        regex ""
        fixed "--fixed-strings"
        matching ""
        non_matching "--invert-match"
        respect_case ""
        ignore_case "--ignore-case"
    )
    for base in g rg; do
        for engine in regex fixed; do
            for caze in respect_case ignore_case; do
                for mode in matching non_matching; do
                    alias "$base$rg_suffix[$engine]$rg_suffix[$caze]$rg_suffix[$mode]"="command $rg_expns[$base] $rg_expns[$engine] $rg_expns[$caze] $rg_expns[$mode]"
                done
            done
        done
    done

    function rtodo() {
        [[ -f TODO ]] && bat -lmd TODO
        [[ -f TODO-PRIV ]] && bat -lmd TODO-PRIV
        command rg --json --hidden --ignore-global --glob '!.git' --ignore-files --ignore-case TODO "$@" | delta
        true
    }

    # use borg's completion for ma-borg
    [ -x /usr/bin/borg ] && compdef ma-borg=borg
fi

# Docker/Podman ($CONTAINER_ENGINE set in .profile)
alias dlf="$CONTAINER_ENGINE logs --follow"
alias dsh="$CONTAINER_ENGINE exec -it"

alias dcb="$CONTAINER_ENGINE-compose build"
alias dcu="$CONTAINER_ENGINE-compose up"
alias dcd="$CONTAINER_ENGINE-compose down -t 120"

function run-container() {
    img="$1"
    shift
    "$CONTAINER_ENGINE" run -it --rm \
        -v ".:/pwd" -v "$HOME/.tmp:/host-tmp" \
        --cap-add sys_chroot \
        "$@" "$img"
}

alias   arch="run-container docker.io/library/archlinux:latest"
alias alpine="run-container docker.io/library/alpine:latest"
alias debian="run-container docker.io/library/debian:unstable"
alias fedora="run-container docker.io/library/fedora:latest"
alias  nixos="run-container docker.io/nixos/nix:latest"

function pg_container() {
    echo "\$POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-<unset>} (MUST be set)"
    echo "\$POSTGRES_USER: ${POSTGRES_USER:-<defult>} (default: postgres)"
    echo "\$POSTGRES_DB: ${POSTGRES_DB:-<default>} (default: \$POSTGRES_USER)"
    echo "\$POSTGRES_PORT: ${POSTGRES_PORT:-<default>} (default: 5432)"
    if [[ -z "$POSTGRES_PASSWORD" ]]; then
        return 1
    fi
    podman run --rm --publish "${POSTGRES_PORT:-5432}":5432 --name postgres --env=POSTGRES_{PASSWORD,USER,DB} "$@" docker.io/postgres:latest
}

function test-makepkg() {
    "$CONTAINER_ENGINE" run -e EXPORT_PKG=1 -v ".:/pkg" --pull always \
        -v /etc/pacman.d/mirrorlist:/etc/pacman.d/mirrorlist:ro whynothugo/makepkg
}

function podman-from-user() {
    podman image scp "${1:?"Missing user"}@localhost::${2:?"Missing image"}" root@localhost::
}

function podman-to-server() {
    podman image scp "${1:?"Missing image"}" "${2:?"Missing user@host:port"}::"
}

# Kittens
# use functions, because kitty's completion script is broken
function icat() {
    kitty +kitten icat "$@"
}
function kdiff() {
    kitty +kitten diff
}

# Man
function man-about() {
    echo "MAN ABBR    CHAPTERS"
    echo "1   sh      Executable programs or shell commands"
    echo "2   sys     System calls (functions provided by the kernel)"
    echo "3   lib     Library calls (functions within program libraries)"
    echo "3p  posix   POSIX library calls"
    echo "4   dev     Special files (usually found in /dev)"
    echo "5   file    File formats and conventions eg /etc/passwd"
    echo "6   games   Games"
    echo "7   misc    Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)"
    echo "8   admin   System administration commands (usually only for root)"
    echo "9   kern    Kernel routines [Non standard]"
}

alias    man-sh='man 1'
alias   man-sys='man 2'
alias   man-lib='man 3'
alias man-posix='man 3p'
alias   man-dev='man 4'
alias  man-file='man 5'
alias man-games='man 6'
alias  man-misc='man 7'
alias man-admin='man 8'
alias  man-kern='man 9'
alias      manp='man 3p'

function man-section() {
    man -P "${PAGER:-less} '+/^$2'" "$1"
}

function serve() {
    if [[ "$1" == "--help" ]]; then
        command serve --help | sed 's/--log-level="info" /--log-level="error"/g'
    else
        command serve -l error "$@" &
        x "http://localhost:8080"
    fi
}

# Timezones
function tz-to() {
    TZ="$1" date -d "${2:-$(date +'%T')} $(date +'%Z')" +'%H:%M %Z'
}

function tz-from() {
    date -d "$2 $(TZ="$1" date +'%z')" +'%H:%M %Z'
}

function tz-alias() {
    if [[ -e "/usr/share/zoneinfo/$2" ]]; then
        alias "tz-to-$1=tz-to $2"
        alias "tz-from-$1=tz-from $2"
    else
        echo "Unknown TZ $2"
    fi
}

tz-alias utc UTC
tz-alias cet Europe/Berlin

tz-alias est America/New_York
tz-alias ny America/New_York
tz-alias pst America/Los_Angeles
tz-alias la America/Los_Angeles

tz-alias sst Asia/Singapore
tz-alias wib Asia/Jakarta

iszsh && unfunction tz-alias

# Cargo
alias cb="cargo build"
alias cr="cargo run"
alias ct="cargo test"
alias ca="cargo add"
alias ca!="cargo add --no-default-features"
alias cdo="cargo doc --no-deps --all-features"
alias cdop="cargo doc --no-deps --all-features --open"
alias cf="cargo fmt"
alias cx="cargo xtask"

function ch() {
    cargo clippy --tests --color always "$@" 2>&1 | less -rF
}

# Sourced scripts
function clone() {
    command clone "$@" | . /dev/stdin
}

function bbclone() {
    command bbclone "$@" | . /dev/stdin
}

function ghclone() {
    command ghclone "$@" | . /dev/stdin
}

function glclone() {
    command glclone "$@" | . /dev/stdin
}

function gcc-arch() {
    command gcc-arch "$@" | . /dev/stdin
}

function java-switch() {
    command java-switch "$@" | . /dev/stdin
}

function switch-java() {
    command switch-java "$@" | . /dev/stdin
}

function clone-arch-pkg() {
    command clone-arch-pkg "$@" | . /dev/stdin
}

if iszsh; then
    # ZSH specific
    alias resh="exec zsh -1"

    if [[ -e /usr/bin/xdg-user-dir ]]; then
        # XDG Directories
        hash -d  desk="$(xdg-user-dir DESKTOP)"
        hash -d downl="$(xdg-user-dir DOWNLOAD)"
        hash -d  docs="$(xdg-user-dir DOCUMENTS)"
        hash -d music="$(xdg-user-dir MUSIC)"
        hash -d  pics="$(xdg-user-dir PICTURES)"
        hash -d  vids="$(xdg-user-dir VIDEOS)"
    fi

    # Util Directories
    hash -d    tmp="$HOME/.tmp"
    hash -d    cfg="${XDG_CONFIG_HOME:=$HOME/.config}"
    hash -d    bin="$HOME/.local/bin"
    hash -d   apps="$HOME/.local/share/applications"
    hash -d  comps="$HOME/.local/share/zsh-functions/"
    hash -d gopath="$GOPATH/src"

    # Dotfiles Directories
    hash -d  zsh="$HOME/.config/zsh"
    hash -d  vim="$HOME/.config/nvim"
    hash -d  awe="$HOME/.config/awesome"
    hash -d   em="$HOME/.emacs.d"
    hash -d doom="$XDG_CONFIG_HOME/doom"

    # Ma_124's Directories
    hash -d     src="$HOME/src/ma124"
    hash -d   gosrc="$GOPATH/src/gitlab.com/Ma_124"
    hash -d tikisrc="$GOPATH/src/bitbucket.org/TIKI-Institut"
else
    #  non-ZSH specific
    alias resh='exec "$SHELL"'
fi
