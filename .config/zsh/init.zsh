# Profile ZSH (uncomment last line as well)
#zmodload zsh/zprof

function iszsh() {
    [[ -n "$zsh_eval_context" ]]
}

[[ -e "$HOME/.config/zsh/custom.zsh" ]] && source "$HOME/.config/zsh/custom.zsh"

# launch another shell for interactive sessions (for example elvish)
if [[ -n "$ZSH_OTHER_INTERACTIVE" && -z "$ZSH_NO_OTHER_INTERACTIVE" ]]; then
    if [[ -x $ZSH_OTHER_INTERACTIVE ]]; then
        exec "$ZSH_OTHER_INTERACTIVE"
    else
        echo "$ZSH_OTHER_INTERACTIVE not executed, because it is not executable"
        echo "make sure it is an absolute path"
    fi
fi

if [[ -x /usr/bin/bat ]]; then
    export MANPAGER="sh -c 'col -bx | bat -l man -p'"
    export MANROFFOPT="-c"

    alias cat="bat -ppS"
fi

# Only execute if sourced by zsh
if iszsh; then
    fpath+="$HOME/.local/share/zsh-functions"

    # Treat - and _ as the same in completion
    HYPHEN_INSENSITIVE="${HYPHEN_INSENSITIVE:-true}"

    # Display ... while waiting for completion
    COMPLETION_WAITING_DOTS="${COMPLETION_WAITING_DOTS:-true}"

    # History file
    HISTFILE="$HOME/.zsh_history"

    # Timestamps for the history command
    HIST_STAMPS="${HIST_STAMPS:-dd.mm.yyyy}"

    # History kept in memory
    HISTSIZE="2000"

    # History kept on disk
    SAVEHIST="100000"

    # Theme
    if starship -V > /dev/null; then
       eval "$(starship init zsh)"
    else
        # Configure Prompt (see man zshmisc)
        if [[ -n "$SSH_CLIENT" ]]; then
            SSH_PROMPT='%F{red}%m%f '
        else
            SSH_PROMPT=''
        fi
        PROMPT="$SSH_PROMPT"'%F{cyan}%1~%f%(0?.. %F{red}%?%f)%(1j. %F{blue}%j%f.) %(!.%F{red}#.%F{green}$)%f '
    fi

    # Enable Extended Globs
    setopt extended_glob

    # Load ZSH completions
    autoload -U compinit && compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"

    # Load better rename
    autoload zmv

    # Load Zsh Syntax Highlighting
    if [[ -e "${ZSH_PLUGIN_SYNTAX_HIGHLIGHTING:=/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh}" ]]; then
        source "$ZSH_PLUGIN_SYNTAX_HIGHLIGHTING"
    fi

    # Load Zsh Autosuggestions
    if [[ -e "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" ]]; then
        source "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
    elif [[ -e "/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh" ]]; then
        source "/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
    fi

    if [[ -e "${ZSH_PLUGIN_HISTORY_SUBSTRING:=/usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh}" ]]; then
        source "$ZSH_PLUGIN_HISTORY_SUBSTRING"
    fi

    # Load FZF
    if [[ -e "/usr/share/fzf/key-bindings.zsh" ]]; then
        source "/usr/share/fzf/key-bindings.zsh"
    elif [[ -e "/usr/share/fzf/shell/key-bindings.zsh" ]]; then
        source "/usr/share/fzf/shell/key-bindings.zsh"
    fi

    # Enable direnv
    if [[ -x /usr/bin/direnv ]]; then
        eval "$(direnv hook zsh)"
    fi

    # Load SDL_GameControllerDB
    #if [[ -e "${SDL_GAMECONTROLLERCONFIG_TXT:="$HOME/.local/share/gamecontrollerdb.txt"}" ]]; then
    #    export SDL_GAMECONTROLLERCONFIG="$(grep -E '^[^#].*Linux' "$SDL_GAMECONTROLLERCONFIG_TXT")"
    #fi

    if [[ -e "${OPAM_INIT_ZSH:=$HOME/.opam/opam-init/init.zsh}" ]]; then
        source "$OPAM_INIT_ZSH"
    fi
fi

# Source all other .zsh files in this directory
find "$HOME/.config/zsh" -maxdepth 1 -name '*.zsh' '!' '(' -name "init.zsh" -or -name "custom.zsh" ')' | sort -g \
| while read _f; do
    source "$_f"
done

# Source all .zsh files in local/ which start with the hostname (case insensitive)
[ -d "$HOME/.config/zsh/local" ] && find "$HOME/.config/zsh/local" -maxdepth 1 -iname "$HOST*.zsh" | sort -g \
| while read _f; do
    source "$_f"
done

#zprof
