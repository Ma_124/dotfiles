-- set auto_start only once per X session
local started_xprop = "MA_AWESOME_STARTED"
awesome.register_xproperty(started_xprop, "boolean")
local auto_start = not awesome.get_xproperty(started_xprop)
awesome.set_xproperty(started_xprop, true)

-- unset auto_start if awesome was started by awesome-noauto
if gears.filesystem.file_readable("/tmp/awesome-started") then
  auto_start = false
  awful.spawn("rm /tmp/awesome-started")
end

if auto_start then
  awful.spawn("kitty --single-instance --detach --class initial-kitty")

  awful.spawn("keepassxc")

  awful.spawn("discord")
  awful.spawn("element-desktop")
  --awful.spawn("cantata")
end

return {
  -- float by default
  {
    properties = { floating = true },

    rule_any = {
      class = {
        "Tor Browser",
      },
      role = {
        "pop-up",
      },
    },
  },

  -- float and ontop by default
  {
    properties = {
      floating = true,
      ontop = true,
    },

    rule_any = {
      class = {
        "pavucontrol-qt",
        "pavucontrol-gtk",
        "spectacle",
        "Xephyr",
      },
      name = {
        "Event Tester", -- xev
      },
    },
  },

  -- float, ontop, and sticky by default
  {
    properties = {
      floating = true,
      ontop = true,
      sticky = true,
    },

    rule_any = {
      class = {
        "Qalculate-gtk",
      },
      name = {
        "Picture-in-Picture", -- Firefox
        "Picture in picture", -- Firefox
      },
    },
  },

  -- never start windows maximized
  {
    rule = { maximized = true },
    properties = { maximized = false },
  },

  {
    rule_any = { class = { "discord", "Element" } },
    properties = { screen = screen_secondary, tag = "9", maximized = false },
  },
  {
    rule_any = { class = { "steam" }, name = { "Steam" } },
    properties = { screen = screen_secondary, tag = "8", maximized = false },
  },
  {
    rule_any = { class = { "initial-kitty" } },
    properties = { screen = screen_secondary, tag = "2", maximized = false },
  },
}
