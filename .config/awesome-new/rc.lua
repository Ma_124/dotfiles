-- awesome_mode: api-level=4:screen=off
-- More info about modeline: https://awesomewm.org/apidoc/documentation/09-options.md.html

--- Imports
-- use LuaRocks if installed
pcall(require, "luarocks.loader")

awful = require("awful")
beautiful = require("beautiful")
gears = require("gears")
naughty = require("naughty")
wibox = require("wibox")

-- TODO: https://github.com/Drauthius/awesome-sharedtags

--- Error handling
-- show notifications when the main configuration failed
-- and this configuration was used as a fallback
if awesome.startup_errors then
    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "AwesomeWM Startup Error",
      text = awesome.startup_errors,
    })
end

-- show notifications for runtime errors
do
  local in_error_handler = false
  awesome.connect_signal("debug::error", function(message)
    -- prevent errors in the handler from starting an infinite loop
    if in_error_handler then return end
    in_error_handler = true

    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "AwesomeWM Error",
      text = message,
    })

    in_error_handler = false
  end)
end

function log(message)
  naughty.notify({
    preset = naughty.config.presets.low,
    title = "Log",
    text = gears.debug.dump_return(message),
  })
end

--- User Config
home = os.getenv("HOME")
config_dir = gears.filesystem.get_configuration_dir()

ma_config = {
  -- theme = "ma",
  theme = "forest",
  wallpaper = home .. "/Pictures/wallpaper.jpg",
  terminal = os.getenv("TERMINAL_NW") or os.getenv("TERMINAL") or "/usr/bin/xterm",
  visual = os.getenv("VISUAL_NW") or os.getenv("VISUAL") or "false",
  browser = os.getenv("BROWSER") or "false",
  launcher = "/home/ma/.config/rofi/launchers/type-3/launcher.sh",
}

-- modifier key
sup = os.getenv("AWESOME_SUP") or "Mod4"

-- mouse button
btn = {
  -- normal ones
  left = 1,
  middle = 2,
  right = 3,

  -- scroll wheel
  up = 4,
  down = 5,
  wheel_left = 6,
  wheel_right = 7,

  -- other
  fwd = 8,
  bwd = 9,
}

-- list of layouts to cycle through
do
  local l = awful.layout.suit
  awful.layout.layouts = {
    l.fair,
    l.max,
    l.floating,
  }
end

--- Styling
-- load theme
beautiful.init(config_dir .. "themes/" .. ma_config.theme .. "/theme.lua")

--- Screens
screen_primary = nil
screen_secondary = nil

-- do something everytime screens change
local function on_screens_updated()
  -- populate screen variables
  do
    local w = 1920
    local h = 1080
    screen_primary = awful.screen.getbycoord(w*0.5, h*0.5) or 1
    screen_secondary = awful.screen.getbycoord(w*1.5, h*0.5) or screen_primary
  end

  -- force all clients to stay reachable after the screens changed
  for c in pairs(client.get()) do
    awful.placement.no_offscreen(c)
  end
end
screen.connect_signal("property::geometry", on_screens_updated)
screen.connect_signal("list", on_screens_updated)
on_screens_updated()

dofile(config_dir .. "ui.lua")

awful.screen.connect_for_each_screen(function(s)
  -- create tags for each screen
  awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
end)

--- Keybinds
local keys = dofile(config_dir .. "keys.lua")

root.keys(keys.global)

--- Mouse
-- focus follows mouse
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

-- inspired by https://stackoverflow.com/a/30684548 and https://stackoverflow.com/a/52868132
--   using glib.idle_add seems to cause race conditions
pending_focus_under_mouse = false
function focus_under_mouse()
  if not pending_focus_under_mouse then
    pending_focus_under_mouse = true

    local t = timer({ timeout = 0.002 })
    t:connect_signal("timeout", function()
      t:stop()
      pending_focus_under_mouse = false

      local c = mouse.current_client
      if c then
        client.focus = c
      end
    end)
    t:start()
  end
end

-- call focus_under_mouse every
-- time a tag is selected
screen.connect_signal("tag::history::update", function(s)
  focus_under_mouse()
end)
-- or a window changes size
client.connect_signal("property::size", function(c)
  focus_under_mouse()
end)
-- or one of these other things happens
client.connect_signal("unmanage", function(c)
  focus_under_mouse()
end)
client.connect_signal("tagged", function(c)
  focus_under_mouse()
end)
client.connect_signal("untagged", function(c)
  focus_under_mouse()
end)
client.connect_signal("property::hidden", function(c)
  focus_under_mouse()
end)
client.connect_signal("property::minimized", function(c)
  focus_under_mouse()
end)
client.connect_signal("property::sticky", function(c)
  focus_under_mouse()
end)

-- manage clients with mouse
local client_buttons = gears.table.join(
  client_buttons,
  awful.button({ sup }, btn.left, function(c)
    c:emit_signal("request::activate", "mouse_click", { raise = true })
    awful.mouse.client.move(c)
  end),
  awful.button({ sup }, btn.right, function(c)
    c:emit_signal("request::activate", "mouse_click", { raise = true })
    awful.mouse.client.resize(c)
  end)
)

-- activate clients when clicked
for b in pairs({ btn.left, btn.middle, btn.right, btn.wheel_left, btn.wheel_right, btn.fwd, btn.bwd }) do
  client_buttons = gears.table.join(
    client_buttons,
    awful.button({ }, b, function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
    end)
  )
end

--- Rules
awful.rules.rules = gears.table.join(
  dofile(config_dir .. "rules.lua"),
  {
    -- apply to all clients
    {
      rule = { },

      properties = {
        border_width = beautiful.border_width,
        border_color = beautiful.border_color,

        focus = awful.client.focus.filter,
        raise = true,

        maximized = false,

        keys = keys.client,
        buttons = client_buttons,

        -- keep clients on same screen, when awesome is restarted
        screen = awful.screen.preferred,
        placement = awful.placement.no_overlap + awful.placement.no_offscreen,
      },
    },

    -- add title bars where appropriate
    {
      properties = { titlebars_enabled = true },

      rule_any = { type = { "normal", "dialog" } },
    },
  }
)
