local keys = {}
local group = ""
local num_groups = 0
local help = "<tt>"
local colors = beautiful.xresources.get_current_theme()

local function grp(name)
  group = name
  num_groups = math.fmod(num_groups + 1, 15)

  help = help .. '<span background="' .. colors["color"..tostring(num_groups)] .. '">' .. name .. "</span>\n"
end

local function def(pat, desc, func)
  local parts = {}
  for part in string.gmatch(pat, "[^-%s]+") do
    table.insert(parts, part)
  end
  local key = table.remove(parts)

  local mods = {}
  for _, part in ipairs(parts) do
    if part == "C" then table.insert(mods, "Control")
    elseif part == "S" then table.insert(mods, "Shift")
    elseif part == "M" then table.insert(mods, sup)
    elseif part == "A" then table.insert(mods, "Mod1")
    else table.insert(mods, part) end
  end

  if func then
    keys = gears.table.join(
      keys,
      awful.key(mods, key, func, { description = desc, group = group })
    )
  end

  local mods = string.gsub(pat, "%s+", "")
  mods = mods .. string.rep(" ", 12-#mods)

  if desc then
    help = help .. '<span foreground="#ff0000">' .. mods .. '</span>' .. desc .. '\n'
  end
end

local help_widget = nil
local function show_help()
  help_widget.visible = true
end

grp("awesome")
def("M-  h", "Show help", require("awful.hotkeys_popup").show_help)
def("M-S-h", "Show help", function() awful.spawn("pango-view --markup /tmp/awesome-help.hack") end)
def("M-  r", "Restart awesome", awesome.restart)
def("M-S-e", "Exit awesome", function() awful.spawn("quit-awesome") end)

grp("launch")
def("M-  Return", "Launch a terminal",          function() awful.spawn(ma_config.terminal) end)
def("M-S-Return", "Launch an editor",           function() awful.spawn(ma_config.visual) end)
def("M-  d",      "Show launcher",              function() awful.spawn(ma_config.launcher) end)
def("M-  l",      "Lock",                       function() awful.spawn("lock") end)
def("M-S-l",      "Lock without pausing media", function() awful.spawn("lock --nopause") end)
def("M-  c",      "Launch a calculator",        function() awful.spawn("qalculate-gtk") end)

grp("screen shot")
def("    Print",  "Screenshot a region",            function() awful.spawn("flameshot gui") end)
def("  S-Print",  "Screenshot the current monitor", function() awful.spawn("flameshot screen") end)
def("  M-Print",  "Screenshot everything",          function() awful.spawn("flameshot full") end)
def("  A-Print",  "Screenshot the current window",  function() awful.spawn("flameshot-window") end)
def("S-A-Print",  "Screenshot the current window",  function() awful.spawn("flameshot-window --ff") end)

grp("focus")
def("M-Tab", "Show last tag", awful.tag.history.restore)
def("A-Tab", "Focus last client", function()
  awful.client.focus.history.previous()
  if client.focus then
    client.focus:raise()
  end
end)
def("M-o",   "Move cursor to other screen", function()
  awful.screen.focus_relative(1)
end)

grp("layout")
def("C-M-d", "Switch to default layout", function()
  awful.layout.set(awful.layout.layouts[1])
end)
def("C-M-m", "Switch to monocle layout", function()
  awful.layout.set(awful.layout.suit.max)
end)
def("C-M-f", "Switch to floating layout", function()
  awful.layout.set(awful.layout.suit.floating)
end)


grp("other")
def("XF86AudioRaiseVolume", nil, function() os.execute("playerctl-helper raise") end)
def("XF86AudioLowerVolume", nil, function() os.execute("playerctl-helper lower") end)
def("XF86AudioMute",        nil, function() os.execute("playerctl-helper toggle-mute") end)
def("XF86AudioNext",        nil, function() os.execute("playerctl-helper next") end)
def("XF86AudioPrev",        nil, function() os.execute("playerctl-helper prev") end)

def("  XF86AudioPlay",      nil, function() os.execute("playerctl-helper toggle-pause") end)
def("S-XF86AudioPlay",      nil, function() os.execute("playerctl-helper pause-all") end)
def("M-p",   "Play/Pause media", function() os.execute("playerctl-helper toggle-pause") end)
def("M-S-p", "Pause all media",  function() os.execute("playerctl-helper pause-all") end)

local global = keys

keys = {}

grp("client")
def("M-S-q", "Close the window",  function(c) c:kill() end)
def("M-S-m", "Move window to master", function(c) c:swap(awful.client.getmaster()) end)
def("M-S-o", "Move to other screen",  function(c) c:move_to_screen() end)

def("M-f",   "Toggle fullscreen", function(c)
  c.fullscreen = not c.fullscreen
  c:raise()
end)
def("M-S-f", "Toggle floating", awful.client.floating.toggle)
def("M-S-t", "Toggle always-on-top",  function(c) c.ontop = not c.ontop end)
def("M-S-s", "Toggle sticky",         function(c) c.sticky = not c.sticky end)
def("M-S-x", "Toggle maximize",       function(c) c.maximized = not c.maximized end)


local client = keys

for i = 1, 9 do
  global = gears.table.join(
    global,
    awful.key({ sup }, "#" .. i + 9, function()
      local s = awful.screen.focused()
      local t = s.tags[i]
      if t then
        t:view_only()
      end
    end),
    awful.key({ sup, "Control" }, "#" .. i + 9, function()
      local s = awful.screen.focused()
      local t = s.tags[i]
      if t then
          awful.tag.viewtoggle(t)
      end
    end)
  )

  client = gears.table.join(
    client,
    awful.key({ sup, "Shift" }, "#" .. i + 9, function(c)
      local t = c.screen.tags[i]
      if t then
        c:move_to_tag(t)
      end
    end),
    awful.key({ sup, "Control", "Shift" }, "#" .. i + 9, function(c)
      local t = c.screen.tags[i]
      if t then
        c:toggle_tag(t)
      end
    end)
  )
end

for _, dir in ipairs({ "Up", "Down", "Left", "Right" }) do
  client = gears.table.join(
    client,
    awful.key({ sup }, dir, function()
      awful.client.focus.bydirection(string.lower(dir), c)
    end),
    awful.key({ sup, "Shift" }, dir, function()
      awful.client.swap.bydirection(string.lower(dir), c)
    end)
  )
end

help = help .. "</tt>"

-- TODO fix this dirty hack and use a proper widget
io.output("/tmp/awesome-help.hack")
io.write(help)
io.close()

return {
  global = global,
  client = client,
}
