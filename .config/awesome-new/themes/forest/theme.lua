local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local base_path   = gfs.get_configuration_dir() .. "/themes/forest"

-- inherit default theme
local theme = dofile(themes_path .. "default/theme.lua")
theme.wallpaper = base_path .. "/wallpaper.jpg"

--local font_sans = "Fira Sans "
--local font_mono = "Fira Code "
local font_sans = "sans-serif "
local font_mono = "monospace "

theme.font          = font_sans .. "10"

-- theme.bg_normal  = "#1b5e20"
theme.bg_normal     = "#00000055"
theme.bg_focus      = "#00000066"
theme.bg_urgent     = "#f57c0055"
theme.bg_minimize   = "#00000000"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#999999"

theme.useless_gap       = dpi(10)
theme.gap_single_client = true
theme.corner_radius     = dpi(0) -- requires ibhagwan/picom until it is merged into yshui/picom
theme.border_width      = dpi(0)
theme.border_normal     = "#FF0000"
theme.border_focus      = "#FF0000"
theme.border_marked     = "#FF0000"

theme.hotkeys_bg               = "#000000ff"
theme.hotkeys_fg               = "#ffffffff"
theme.hotkeys_modifiers_fg     = "#512f05ff"
theme.hotkeys_font             = font_mono .. "13"
theme.hotkeys_description_font = font_mono .. "13"

theme.notification_bg        = "#00000055"
theme.notification_font      = font_sans .. "13"
theme.notification_max_width = dpi(400)
theme.notification_icon_size = dpi(100)

theme.tooltip_opacity = "#00000088"

theme.systray_icon_spacing = dpi(5)
theme.taglist_spacing = dpi(2)

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]

return theme
