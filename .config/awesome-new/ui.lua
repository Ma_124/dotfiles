--- Wallpaper
local function set_wallpaper(s, w, log_error)
  if not gears.filesystem.file_readable(w) then
    local msg = "The wallpaper file at " .. w .. " is not readable."
    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "Wallpaper Not Found",
      text = msg,
    })
    print(msg)
  end

  if s then
    gears.wallpaper.maximized(w, s)
  end
end

for s in screen do
  set_wallpaper(s, ma_config.wallpaper, i == 1)
end
screen.connect_signal("request::wallpaper", function(s) set_wallpaper(s, ma_config.wallpaper, false) end)

--- Title Bar
client.connect_signal("request::titlebars", function(c)
  local w = awful.titlebar.widget
  local l = wibox.layout

  local buttons = gears.table.join(
    awful.button({ }, btn.left, function()
      c:emit_signal("request::activate", "titlebar", { raise = true })
      awful.mouse.client.move(c)
    end),
    awful.button({ }, btn.right, function()
      c:emit_signal("request::activate", "titlebar", { raise = true })
      awful.mouse.client.resize(c)
    end)
  )

  awful.titlebar(c) : setup({
    layout = l.align.horizontal,
    {
      layout = l.fixed.horizontal,
      w.iconwidget(c),
    },
    {
      layout = l.flex.horizontal,
      {
        widget = w.titlewidget(c),
        align = "center",
      },
    },
    {
      layout = l.fixed.horizontal,
    },
    buttons = buttons,
  })
end)

-- border
--client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
--client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

--- Panel / Bar
-- bar widgets that are not specific to a screen
local ma_clock = wibox.widget.textclock("%a %d.%m.%Y  %H:%M  ")

-- there can only be one systray
local ma_systray = wibox.widget.systray()

-- buttons to be used for screen specific widgets
local ma_tags_buttons = gears.table.join(
  awful.button({ }, btn.left, function(t) t:view_only() end),
  awful.button({ }, btn.right, function(t) t:view_toggle() end),

  awful.button({ "Shift "}, btn.left, function(t)
    if client.focus then
      client.focus:move_to_tag(t)
    end
  end),
  awful.button({ "Shift "}, btn.right, function(t)
    if client.focus then
      client.focus:toggle_tag(t)
    end
  end)
)

local ma_clients_buttons = gears.table.join(
  awful.button({ }, btn.left, function(c)
    if c == client.focus then
      c.minimize = true
    else
      c:emit_signal("request::activate", "tasklist", { raise = true })
    end
  end),
  awful.button({ }, btn.right, function(c)
    awful.menu.client_list({ theme = { width = 256 } })
  end)
)

-- do something once for each screen
awful.screen.connect_for_each_screen(function(s)
  --- Per screen styling
  local l = wibox.layout

  -- create a list of all tags
  s.ma_tags = awful.widget.taglist({
    screen = s,
    filter = awful.widget.taglist.filter.all,
    buttons = ma_tags_buttons,
  })

  -- create a list of clients on the current workspace
  s.ma_clients = awful.widget.tasklist({
    screen = s,
    filter = awful.widget.tasklist.filter.currenttags,
    buttons = ma_clients_buttons,
  })

  -- other screen specific widgets
  s.ma_layout_indicator = awful.widget.layoutbox(s)
  s.ma_layout_indicator : buttons(gears.table.join(
    awful.button({ }, btn.left,  function() awful.layout.inc(1)  end),
    awful.button({ }, btn.right, function() awful.layout.inc(-1) end)
  ))

  local possibly_systray = nil
  if screen.primary == s then
    possibly_systray = ma_systray
  end

  -- create a bar
  s.ma_bar = awful.wibar({ position = "bottom", screen = s })
  s.ma_bar : setup({
    layout = l.align.horizontal,
    {
      layout = l.fixed.horizontal,
      s.ma_tags,
    },
    s.ma_clients,
    {
      layout = l.fixed.horizontal,
      possibly_systray,
      ma_clock,
      s.ma_layout_indicator,
    }
  })
end)
