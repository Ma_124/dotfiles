# Firefox Configuration

The actual configuration files for firefox live in `.mozilla/firefox/<profile>/`.
A [`./configure-firefox`](./configure-firefox) script is provided in this directory to symlink the configuration files.

## Usage
`~cfg/firefox/configure-firefox <profile name>`

