// this is not real javascript. firefox only allows calls to user_pref and comments.

// TODO: remove when fixed
// don't use the xdg portals as they seem broken for the moment
user_pref("widget.use-xdg-desktop-portal.file-picker", 0);

// don't show warning when opening about:config
user_pref("browser.aboutConfig.showWarning", false);

// allow user to install extensions not signed by mozilla
// NOTE: with this set to false you need to be extra careful what extensions
//       you install. You should only set it to false if you use your own extensions
// NOTE: this only works in FF Developer Edition and is silently ignored otherwise
user_pref("xpinstall.signatures.required", false);

// Enable hardware video acceleration
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.rdd-ffmpeg.enabled", true);

// pressing BACKSPACE navigates backward through history
user_pref("browser.backspace_action", 0);

// set home and new tab page
// https://home-srv-0.lan/ma/bento/
user_pref("browser.startup.homepage", "https://example.org");
// removed by firefox for "security" reasons: user_pref("browser.newtab.url", "https://example.org");

// ask user where to save files
user_pref("browser.download.useDownloadDir", false);

// restore previous session when starting
user_pref("browser.startup.page", 3);

// don't warn if closing a window with multiple tabs
user_pref("browser.tabs.warnOnClose", false);

// don't close windows when closing last tab in that window
user_pref("browser.tabs.closeWindowWithLastTab", false);

// always hide bookmarks toolbar
user_pref("browser.toolbars.bookmarks.visibility", "never");

// dock devtools on the right
user_pref("devtools.toolbox.host", "right");

// hide all matches with Ctrl-F
user_pref("findbar.highlightAll", true);

// by default match ä as a but provide a button for it
user_pref("findbar.matchdiacritics", 0);

// don't show fullscreen warning
user_pref("full-screen-api.warning.timeout", -1);

// by default print to PDF rather than remembering last printer
user_pref("print_printer", "Mozilla Save to PDF");

// use xdg desktop portal dialogues such as KDE's Save dialogue.
// depends on xdg-desktop-portal or xdg-desktop-portal-kde on Arch
user_pref("widget.use-xdg-desktop-portal", true);

// use os locales (change input mm/dd/yyyy to proper format)
user_pref("intl.regional_prefs.use_os_locales", true);

// disable JS in .pdf files
user_pref("pdfjs.enableScripting", false);

// don't check whether firefox is the default browser
user_pref("browser.shell.checkDefaultBrowser", false);

// use hex as default color unit
user_pref("devtools.defaultColorUnit", "hex");

// I am hackerman
user_pref("devtools.everOpened", true);

// my eyes!
user_pref("devtools.theme", "dark");

// devtools on righthand side
user_pref("devtools.toolbox.host", "right");
user_pref("devtools.toolbox.previousHost", "window");

// don't use my search engine for domains ending in .lan
user_pref("browser.fixup.domainsuffixwhitelist.lan", true);

// just open like a normal window, so WM rules apply
user_pref("widget.disable-workspace-management", true);

// Disable most stuff on the default new tab page
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.activity-stream.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeBookmarks", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeDownloads", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeVisited", false);
user_pref("browser.newtabpage.activity-stream.showSearch", false);

// Disable telemetry (https://wiki.webevaluation.nl/firefox_privacy_and_security)
user_pref("datareporting.healthreport.service.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.ping-centre.telemetry", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.shutdownPingSender.enabledFirstSession", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("beacon.enabled", false);
user_pref("browser.discovery.enabled", false);

// Disable geolocation (0100::/64 is the IPv6 blackhole prefix)
user_pref("geo.enabled", false);
user_pref("geo.wifi.logging.enabled", false);
user_pref("geo.wifi.uri", "[0100::]");
user_pref("geo.provider.network.url", "[0100::]");
