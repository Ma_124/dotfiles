# -*- gdb-script -*- vim: ft=gdb:

# For example:
define hook-stop
  #disassemble
  x/3i $rip
  #x/20gx $rsp
end
