# -*- gdb-script -*- vim: ft=gdb:

alias bp = break
alias bpl = info breakpoints
alias bpc = clear
alias bpe = enable
alias bpd = disable
alias tbp = tbreak

define ct
  tbreak $arg0
  c
end

alias bpm = back -past-main

alias xg1 = x/gx
alias xg = x/64gx
alias xxg = x/128gx

alias xw = x/128wx
alias xxw = x/256wx

alias xb = x/128bx
alias xs = x/10s
alias xi = x/30i

alias aslr=set disable-randomization off
