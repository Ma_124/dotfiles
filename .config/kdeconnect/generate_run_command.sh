#!/bin/zsh

(
    printf '[General]\ncommands="@ByteArray(';
    cat ~/.config/kdeconnect/run_command_raw.json | \
        python3 -m json.tool --compact | \
        sed -E 's/"/\\"/g'; \
        printf ')"'
) > ~/.config/kdeconnect/9657f21a5046a13b/kdeconnect_runcommand/config
